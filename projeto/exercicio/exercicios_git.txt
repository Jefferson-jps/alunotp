Exercícios – Git

Crie um novo repositório local com um projeto à sua escolha e realize pelo menos dois commits neste repositório

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/Projeto_Dino
$ git init
Initialized empty Git repository in C:/Users/kieling.Lenovo-PC/Desktop/Projeto_Dino/.git/

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/Projeto_Dino (master)
$ git add .

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/Projeto_Dino (master)
$ git commit -m "Criação dos arquivos"
[master (root-commit) d3f95bd] Criação dos arquivos
 7 files changed, 130 insertions(+)
 create mode 100644 img/background.png
 create mode 100644 img/cactus.png
 create mode 100644 img/dino.png
 create mode 100644 img/example.png
 create mode 100644 index.html
 create mode 100644 script.js
 create mode 100644 style.css

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/Projeto_Dino (master)
$ git status
On branch master
nothing to commit, working tree clean

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/Projeto_Dino (master)
$ git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   index.html

no changes added to commit (use "git add" and/or "git commit -a")

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/Projeto_Dino (master)
$ git commit -m "Alteração no titulo do index"
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   index.html

no changes added to commit (use "git add" and/or "git commit -a")

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/Projeto_Dino (master)
$ git add .

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/Projeto_Dino (master)
$ git status
On branch master
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   index.html


Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/Projeto_Dino (master)
$ git remote add origin https://gitlab.com/Jefferson-jps/projeto_dino.git

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/Projeto_Dino (master)
$ git push -u origin
fatal: The current branch master has no upstream branch.
To push the current branch and set the remote as upstream, use

    git push --set-upstream origin master


Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/Projeto_Dino (master)
$ git push --set-upstream origin master
Enumerating objects: 10, done.
Counting objects: 100% (10/10), done.
Delta compression using up to 4 threads
Compressing objects: 100% (10/10), done.
Writing objects: 100% (10/10), 26.76 KiB | 2.43 MiB/s, done.
Total 10 (delta 0), reused 0 (delta 0), pack-reused 0
To https://gitlab.com/Jefferson-jps/projeto_dino.git
 * [new branch]      master -> master
Branch 'master' set up to track remote branch 'master' from 'origin'.

Link do repositório no GitLab: Jefferson Silva / Projeto_Dino · GitLab 

Escolha um repositório disponível no gitlab.com e faça a clonagem do mesmo, identificando qual foi o autor do último commit realizado no projeto e a(s) linguagem(s) utilizadas.

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/workspace-tecnicas_programacao
$ git clone https://gitlab.com/rVenson/exercicio_git.git
Cloning into 'exercicio_git'...
remote: Enumerating objects: 8, done.
remote: Counting objects: 100% (8/8), done.
remote: Compressing objects: 100% (8/8), done.
remote: Total 8 (delta 1), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (8/8), 10.55 KiB | 93.00 KiB/s, done.
Resolving deltas: 100% (1/1), done.

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/workspace-tecnicas_programacao
$ cd exercicio_git/

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/workspace-tecnicas_programacao/exercicio_git (master)
$ git log --stat
commit 0efcb6a2d845016d412745303b048e7d5396baea (HEAD -> master, origin/master, origin/HEAD)
Author: rvenson <ramon.venson@gmail.com>
Date:   Fri Sep 25 04:29:05 2020 -0300

    Base do projeto

 img/under_construction.jpg | Bin 0 -> 9470 bytes
 index.html                 |  19 +++++++++++++
 index.js                   |  26 ++++++++++++++++++
 profile.html               |  12 +++++++++
 style.css                  |  65 +++
Tecnologias utilizadas neste projeto: Html, Css e JS.
Link do repositório: Ramon Venson / exercicio_git · GitLab

Identifique a finalidade dos seguintes comandos:

a) git init – Criar um novo repositório

b) git config --global user.name "turing" – Configuar/setar um usuário, neste caso o usuário “turing”

c) git add EXERCICIO.txt  - Adiciona um arquivo em especifico, neste caso o EXERCICIO.txt

d) git add . – Adicionar todos os arquivos/diretórios

e) git commit -m "Adicionado nova interface" – Comitar um arquivo informando uma mensagem/comentário

f) git commit – Comitar um arquivo

g) git reset --hard HEAD – Descartar as alterações na área de stage como também reverte as alterações no diretório de working para o estado de commit que foi especificado no comando.

h) cd Downloads – Acessa a pasta Downloads

i) pwd – Mostrar o diretório de trabalho atual

j) cd .. – Move uma pasta acima

k) ls – Listar o conteúdo do diretório de trabalho atual

l) git pull – Atualizar os arquivos no branch atual

m) git push  - Enviar arquivos/diretórios para o repositório remoto

n) git clone https://gitlab.com/rVenson/linguagemdeprogramacao - Clonar um repositório remoto

o) git diff – Realiar uma função de comparação nas fontes de dados Git.

p) git show – Exibe vários tipos de objetos

Descreva a função dos seguintes componentes do Git 

1) Stage Area, Commit – Permite que continuemos fazendo alterações em nosso diretório de trabalho e, quando decidirmos que queremos interagir com o controle de versão, permite que guarde as mudanças em pequenos commits.

2) Local Repository – É um repositório que só existe em nosso computador local.

3) Remote Repository – Podemos utilizar um repositório remoto para salvar nossos arquivos de modo que possamos consulta-los ou edita-los de um outro computador diferente do computador local 

Exercício 2

Crie seu próprio repositório no GitLab denominado AlunoTP. 
Crie uma pasta chamada projeto e exercícios e na pasta exercícios inclua a resolução dos exercícios Git. 
Envie o commit para o repositório remoto. 
Adicione um arquivo denominado README.md ao projeto e inclua o seguinte conteúdo 
Repositório de exercícios das aulas de Técnicas de Programação 
Adicione uma nova pasta dentro da pasta exercícios chamado exercicio_java. 
Busque um exercício que você já realizou e suba para o repositório. 
Crie também um arquivo chamado README.md na pasta raiz da pasta e inclua o cabeçalho deste exercício

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/workspace-tecnicas_programacao/alunotp (master)
$ cd ..

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/workspace-tecnicas_programacao
$ git clone https://gitlab.com/Jefferson-jps/alunotp.git
Cloning into 'alunotp'...
warning: You appear to have cloned an empty repository.

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/workspace-tecnicas_programacao
$ cd alunotp/

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/workspace-tecnicas_programacao/alunotp (master)
$ git status
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        projeto/

nothing added to commit but untracked files present (use "git add" to track)

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/workspace-tecnicas_programacao/alunotp (master)
$ ls
projeto/

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/workspace-tecnicas_programacao/alunotp (master)
$ cd projeto

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/workspace-tecnicas_programacao/alunotp/projeto (master)
$ ls
exercicio/

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/workspace-tecnicas_programacao/alunotp/projeto (master)
$ cd exercicio/

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/workspace-tecnicas_programacao/alunotp/projeto/exercicio (master)
$ ls
 Exercícios_Git.docx   exercicios_git.txt  '~$ercícios_Git.docx'

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/workspace-tecnicas_programacao/alunotp/projeto/exercicio (master)
$ cd ..

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/workspace-tecnicas_programacao/alunotp/projeto (master)
$ cd ..

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/workspace-tecnicas_programacao/alunotp (master)
$ git status
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        projeto/

nothing added to commit but untracked files present (use "git add" to track)

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/workspace-tecnicas_programacao/alunotp (master)
$ git commit -m "Primeiro commit"
On branch master

Initial commit

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        projeto/

nothing added to commit but untracked files present (use "git add" to track)

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/workspace-tecnicas_programacao/alunotp (master)
$ git add .

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/workspace-tecnicas_programacao/alunotp (master)
$ git commit -m "Primeiro Commit"
[master (root-commit) 8f9a61d] Primeiro Commit
 4 files changed, 167 insertions(+)
 create mode 100644 "projeto/exercicio/Exerc\303\255cios_Git.docx"
 create mode 100644 "projeto/exercicio/Exerc\303\255cios_Git.pdf"
 create mode 100644 projeto/exercicio/exercicios_git.txt
 create mode 100644 "projeto/exercicio/~$erc\303\255cios_Git.docx"

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/workspace-tecnicas_programacao/alunotp (master)
$ cd projeto/

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/workspace-tecnicas_programacao/alunotp/projeto (master)
$ cd ..

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/workspace-tecnicas_programacao/alunotp (master)
$ git add .

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/workspace-tecnicas_programacao/alunotp (master)
$ git status
On branch master
Your branch is based on 'origin/master', but the upstream is gone.
  (use "git branch --unset-upstream" to fixup)

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        new file:   projeto/README.md
        deleted:    "projeto/exercicio/Exerc\303\255cios_Git.docx"
        new file:   projeto/exercicio/exercicio_java/Exercicio_Aula04/build.xml
        new file:   projeto/exercicio/exercicio_java/Exercicio_Aula04/build/classes/.netbeans_automatic_build
        new file:   projeto/exercicio/exercicio_java/Exercicio_Aula04/build/classes/.netbeans_update_resources
        new file:   projeto/exercicio/exercicio_java/Exercicio_Aula04/build/classes/Paciente.class
        new file:   projeto/exercicio/exercicio_java/Exercicio_Aula04/build/classes/TestePaciente.class
        new file:   projeto/exercicio/exercicio_java/Exercicio_Aula04/manifest.mf
        new file:   projeto/exercicio/exercicio_java/Exercicio_Aula04/nbproject/build-impl.xml
        new file:   projeto/exercicio/exercicio_java/Exercicio_Aula04/nbproject/genfiles.properties
        new file:   projeto/exercicio/exercicio_java/Exercicio_Aula04/nbproject/private/private.properties
        new file:   projeto/exercicio/exercicio_java/Exercicio_Aula04/nbproject/private/private.xml
        new file:   projeto/exercicio/exercicio_java/Exercicio_Aula04/nbproject/project.properties
        new file:   projeto/exercicio/exercicio_java/Exercicio_Aula04/nbproject/project.xml
        new file:   projeto/exercicio/exercicio_java/Exercicio_Aula04/src/Paciente.java
        new file:   projeto/exercicio/exercicio_java/Exercicio_Aula04/src/TestePaciente.java
        new file:   projeto/exercicio/exercicio_java/README.md
        deleted:    "projeto/exercicio/~$erc\303\255cios_Git.docx"


Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/workspace-tecnicas_programacao/alunotp (master)
$ git commit -m "Resolução dos exercicios - Git"
[master 44a76d5] Resolução dos exercicios - Git
 18 files changed, 1665 insertions(+)
 create mode 100644 projeto/README.md
 delete mode 100644 "projeto/exercicio/Exerc\303\255cios_Git.docx"
 create mode 100644 projeto/exercicio/exercicio_java/Exercicio_Aula04/build.xml
 create mode 100644 projeto/exercicio/exercicio_java/Exercicio_Aula04/build/classes/.netbeans_automatic_build
 create mode 100644 projeto/exercicio/exercicio_java/Exercicio_Aula04/build/classes/.netbeans_update_resources
 create mode 100644 projeto/exercicio/exercicio_java/Exercicio_Aula04/build/classes/Paciente.class
 create mode 100644 projeto/exercicio/exercicio_java/Exercicio_Aula04/build/classes/TestePaciente.class
 create mode 100644 projeto/exercicio/exercicio_java/Exercicio_Aula04/manifest.mf
 create mode 100644 projeto/exercicio/exercicio_java/Exercicio_Aula04/nbproject/build-impl.xml
 create mode 100644 projeto/exercicio/exercicio_java/Exercicio_Aula04/nbproject/genfiles.properties
 create mode 100644 projeto/exercicio/exercicio_java/Exercicio_Aula04/nbproject/private/private.properties
 create mode 100644 projeto/exercicio/exercicio_java/Exercicio_Aula04/nbproject/private/private.xml
 create mode 100644 projeto/exercicio/exercicio_java/Exercicio_Aula04/nbproject/project.properties
 create mode 100644 projeto/exercicio/exercicio_java/Exercicio_Aula04/nbproject/project.xml
 create mode 100644 projeto/exercicio/exercicio_java/Exercicio_Aula04/src/Paciente.java
 create mode 100644 projeto/exercicio/exercicio_java/Exercicio_Aula04/src/TestePaciente.java
 create mode 100644 projeto/exercicio/exercicio_java/README.md
 delete mode 100644 "projeto/exercicio/~$erc\303\255cios_Git.docx"

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/workspace-tecnicas_programacao/alunotp (master)
$ git status
On branch master
Your branch is based on 'origin/master', but the upstream is gone.
  (use "git branch --unset-upstream" to fixup)

nothing to commit, working tree clean

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/workspace-tecnicas_programacao/alunotp (master)
$ git remote add origin https://gitlab.com/Jefferson-jps/alunotp.git
error: remote origin already exists.

Lenovo-PC+kieling@Lenovo-PC MINGW64 ~/Desktop/workspace-tecnicas_programacao/alunotp (master)
$ git push
Enumerating objects: 34, done.
Counting objects: 100% (34/34), done.
Delta compression using up to 4 threads
Compressing objects: 100% (29/29), done.
Writing objects: 100% (34/34), 269.14 KiB | 1.98 MiB/s, done.
Total 34 (delta 0), reused 0 (delta 0), pack-reused 0
To https://gitlab.com/Jefferson-jps/alunotp.git
 * [new branch]      master -> master

Link do repositório: Jefferson Silva / AlunoTP · GitLab

