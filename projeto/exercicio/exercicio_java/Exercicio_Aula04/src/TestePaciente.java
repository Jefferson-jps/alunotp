
import javax.swing.JOptionPane;

public class TestePaciente {

    public static void main(String[] args) {
        Paciente p = new Paciente();

        p.nome = JOptionPane.showInputDialog("Digite o nome do paciente: ");
        p.rg = JOptionPane.showInputDialog("Digite o rg do paciente: ");
        p.endereco = JOptionPane.showInputDialog("Digite o endereço do paciente: ");
        p.telefone = JOptionPane.showInputDialog("Digite o telefone do paciente: ");
        p.dataNascimento = JOptionPane.showInputDialog("Digite a data de nascimento do paciente: ");
        p.profissao = JOptionPane.showInputDialog("Digite a  profissão do paciente: ");

        JOptionPane.showMessageDialog(null, "Nome: " + p.nome + "\nRg: " + p.rg + "\nEndereço: " + p.endereco + ""
                + "\nTelefone: " + p.telefone + "\nData de Nascimento: " + p.dataNascimento + "\nProfissão: " + p.profissao);
    }

}
